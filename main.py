#!/usr/bin/env python3
import random

'''Napisz funkcję, która pobierze od użytkownika imię i sprawdzi czy jest takie
samo jak twoje. W przypadku gdy jest równoważne wyświetli inne powitanie
niż w sytuacji gdy to imię jest inne.'''


def check_name():
    user_name: str = input("Put your name here: ")
    if user_name == "Andrzej":
        print("Hello")
    else:
        print("Hi")


check_name()

'''Napisz funkcję, która pobierze od użytkownika liczbę i gdy jest to liczba
różna od zera wyświetli ją. Gdy jest to zero wyświetli dowolny napis. Do
sprawdzenia wartości nie używaj operatorów porównania.'''


def check_number():
    user_number = int(input("Please give a number: "))
    if user_number:
        print(user_number)
    else:
        print("The number is equeal to 0")


check_number()

'''Zrób pętle while, która będzie wykonywać się tak długo dopóki zmienna x nie będzie równa 20.
Wykorzystaj funkcję randrange z biblioteki random'''

x = int(0)
attempt = int(0)
while x != 20:
    x = random.randrange(100)
    attempt += 1
print("x found after ", attempt, " attempts")

'''Zrób pętlę for, która wyświetli dane od 20 do 0 korzystającz parametrów funkcji range'''

for number in range(20, -1, -1):
    print(number, end=" ")

print()
'''Napisz klasę dla dowolnej bryły, która będzie posiadała wszystkie funkcje
pozwalające porównywać obiekty tej klasy'''


class Roller:
    def __init__(self, r, h):
        self.r = r
        self.h = h
        self.capacity = 3.14*r*r*h

    def __eq__(self, other):
        return self.capacity == other.capacity

    def __lt__(self, other):
        return self.capacity < other.capacity

    def __gt__(self, other):
        return self.capacity > other.capacity

    def __le__(self, other):
        return self.capacity <= other.capacity

    def __ge__(self, other):
        return self.capacity >= other.capacity

    def __ne__(self, other):
        return self.capacity != other.capacity

'''Napisz klasę dla dowolnej figury płaskiej, która będzie posiadała wszystkie
funkcje pozwalające porównywać obiekty tej klasy.'''


class Rectangle:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.surfaceArea = a*b

    def __eq__(self, other):
        return self.surfaceArea == other.surfaceArea

    def __lt__(self, other):
        return self.surfaceArea < other.surfaceArea

    def __gt__(self, other):
        return self.surfaceArea > other.surfaceArea

    def __le__(self, other):
        return self.surfaceArea <= other.surfaceArea

    def __ge__(self, other):
        return self.surfaceArea >= other.surfaceArea

    def __ne__(self, other):
        return self.surfaceArea != other.surfaceArea

'''Stwórz drzewo klas (min. 6 klas, min. 2 poziomy dziedziczenia), w którym każda klasa
posiada komplet funkcji pozwalających porównywać obiekty tej klasy. Nie może to być drzewo brył
ani drzewo figur płaskich.'''


class WheeledVehicles:
    def __init__(self, brand, model, color):
        self.brand = brand
        self.model = model
        self.color = color

    def __eq__(self, other):
        return (self.brand == other.brand) and (self.model == other.model) and (self.color == other.color)

    def __ne__(self, other):
        return (self.brand != other.brand) and (self.model != other.model) and (self.color != other.color)


class Motorcycle(WheeledVehicles):
    def __init__(self, brand, model, color, engine_capacity):
        super().__init__(brand, model, color)
        self.engine_capacity = engine_capacity

    def __eq__(self, other):
        return super().__eq__(other) and (self.engine_capacity == other.engine_capacity)

    def __ne__(self, other):
        return super().__eq__(other) and (self.engine_capacity != other.engine_capacity)

    def __gt__(self, other):
        return self.engine_capacity > other.engine_capacity

    def __lt__(self, other):
        return self.engine_capacity < other.engine_capacity

    def __le__(self, other):
        return self.engine_capacity <= other.engine_capacity

    def __ge__(self, other):
        return self.engine_capacity >= other.engine_capacity


class Car(WheeledVehicles):
    def __init__(self, brand, model, color, num_of_doors):
        super().__init__(brand, model, color)
        self.num_of_doors = num_of_doors

    def __eq__(self, other):
        return super().__eq__(other) and (self.num_of_doors == other.num_of_doors)

    def __ne__(self, other):
        return super().__eq__(other) and (self.num_of_doors != other.num_of_doors)

    def __gt__(self, other):
        return self.num_of_doors > other.num_of_doors

    def __lt__(self, other):
        return self.num_of_doors < other.num_of_doors

    def __le__(self, other):
        return self.num_of_doors <= other.num_of_doors

    def __ge__(self, other):
        return self.num_of_doors >= other.num_of_doors


class Truck(WheeledVehicles):
    def __init__(self, brand, model, color, permissible_gross_weight):
        super().__init__(brand, model, color)
        self.permissible_gross_weight = permissible_gross_weight

    def __eq__(self, other):
        return super().__eq__(other) and (self.permissible_gross_weight == other.permissible_gross_weight)

    def __ne__(self, other):
        return super().__eq__(other) and (self.permissible_gross_weight != other.permissible_gross_weight)

    def __gt__(self, other):
        return self.permissible_gross_weight > other.permissible_gross_weight

    def __lt__(self, other):
        return self.permissible_gross_weight < other.permissible_gross_weight

    def __le__(self, other):
        return self.permissible_gross_weight <= other.permissible_gross_weight

    def __ge__(self, other):
        return self.permissible_gross_weight >= other.permissible_gross_weight


class SportCar(Car):
    def __init__(self, brand, model, color, num_of_doors, max_speed):
        super().__init__(brand, model, color, num_of_doors)
        self.max_speed = max_speed

    def __eq__(self, other):
        return super().__eq__(other) and (self.max_speed == other.max_speed)

    def __ne__(self, other):
        return super().__eq__(other) and (self.max_speed != other.max_speed)

    def __gt__(self, other):
        return super().__gt__(other) and self.max_speed > other.max_speed

    def __lt__(self, other):
        return super().__lt__(other) and self.max_speed < other.max_speed

    def __le__(self, other):
        return super().__le__(other) and self.max_speed <= other.max_speed

    def __ge__(self, other):
        return super().__ge__(other) and self.max_speed >= other.max_speed


class CityCar(Car):
    def __init__(self, brand, model, color, num_of_doors, fuel_consumption):
        super().__init__(brand, model, color, num_of_doors)
        self.fuel_consumption = fuel_consumption

    def __eq__(self, other):
        return super().__eq__(other) and (self.fuel_consumption == other.fuel_consumption)

    def __ne__(self, other):
        return super().__eq__(other) and (self.fuel_consumption != other.fuel_consumption)

    def __gt__(self, other):
        return super().__gt__(other) and self.fuel_consumption > other.fuel_consumption

    def __lt__(self, other):
        return super().__lt__(other) and self.fuel_consumption < other.fuel_consumption

    def __le__(self, other):
        return super().__le__(other) and self.fuel_consumption <= other.fuel_consumption

    def __ge__(self, other):
        return super().__ge__(other) and self.fuel_consumption >= other.fuel_consumption
